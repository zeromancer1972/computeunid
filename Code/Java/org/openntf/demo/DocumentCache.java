package org.openntf.demo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.openntf.domino.ViewEntry;
import org.openntf.domino.ViewEntryCollection;
import org.openntf.domino.utils.XSPUtil;

public class DocumentCache implements Serializable {

	/**
	 * Members
	 */
	private static final long serialVersionUID = 1L;
	private Map<String, String> names;

	public DocumentCache() {
		init();
	}

	public void init() {
		names = new HashMap<String, String>();
		ViewEntryCollection col = XSPUtil.getCurrentDatabase().getView("ByName").getAllEntries();
		for (ViewEntry ent : col) {
			names.put(ent.getDocument().getItemValueString("unique"), ent.getDocument().getUniversalID());
		}
	}

	public synchronized Map<String, String> getNames() {
		return names;
	}

}
